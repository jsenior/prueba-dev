<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
// $num = 1324223.1365593;

// echo 'dado el numero: '.$num ;
// echo '<br>';
// echo 'Parte entera: '.floor($num);
// echo '<br>';
// $decimal = ltrim(($num - floor($num)),"0.");
// echo 'Parte decimal: '.$decimal;
// echo '<br>';
// echo 'Redondeo a dos decimales: '.round($num,2);
// echo '<br>';
// echo 'Redondeo Superior: '.round($num, 0, PHP_ROUND_HALF_UP);
// echo '<br>';
// echo 'Redondeo Inferior: '.round($num, 0, PHP_ROUND_HALF_DOWN);
// echo '<br>';
// echo 'Numero con formato (dos decimales) ##.###,##: '.number_format($num, 2, ',', '.');;


## 01 - Números

// Dado de número `$num = 1324223.1365593`
$num = 1324223.1365593;

// Obtener:

// - Parte entera
echo (int) $num . PHP_EOL;
// - Parte decimal
echo $num - (int) $num . PHP_EOL;
// - Redondeo a dos decimales
echo round($num) . PHP_EOL;
// - Redondeo Superior
echo ceil($num) . PHP_EOL;
// - Redondeo Inferior
echo floor($num) . PHP_EOL;
// - Numero con formato (dos decimales) ##.###,##
echo number_format($num, 2) . PHP_EOL;
