<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Numbers {

    private static $total;

    static function sum($a, $b) {     
        //self::$array[] = $palabra;
        self::$total = $a + $b;
        echo $a.' + '.$b.' = '.self::$total;
        echo '<br>';
//        return self::$total;
    }
    
     static function diff($a, $b) {        
        self::$total = $a - $b;
        echo $a.' - '.$b.' = '.self::$total;
        echo '<br>';
        //return $total;
    }
    
     static function format($a, $b=2) {        
        
        self::$total = number_format($a, $b, '.', ',');
        return self::$total;
    }
    
     static function money($a) {
        
        setlocale(LC_MONETARY, 'en_US.UTF-8');       
        self::$total = money_format('%(#10n', $a);
        return self::$total;
    }

}

echo Numbers::sum( 3, 5);
echo Numbers::diff( 2, 4);
echo Numbers::format(1262.25361154, 4);
echo '<br>';
echo Numbers::format(1262.25361154);
