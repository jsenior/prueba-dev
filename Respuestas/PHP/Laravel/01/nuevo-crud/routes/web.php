<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ContacController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*Route::get('/contac', function () {
    return view('contac.create');
});

Route::get('/contac', function () {
    return view('contac.index');
});

Route::resource('contact', ContacController::class);

*/
//Route::resource('/contac', 'ContacController');
Route::get('/contac',[ContacController::class,'index']);
Route::get('/contac/create',[ContacController::class,'create']);
Route::post('/contac/respuesta',[ContacController::class,'store']);
Route::delete('/contac/{id}',[ContacController::class,'destroy']);
Route::get('/contac/{id}/edit',[ContacController::class,'edit']);
Route::put('/contac/{id}/edit',[ContacController::class,'update']);




