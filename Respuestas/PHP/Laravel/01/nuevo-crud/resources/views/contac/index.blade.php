Mostrar la lista de contactos

<table class="table table-light">
    <thread class="thread-light">
        <tr>
            <th>id</th>
            <th>Nombre</th>
            <th>Correo</th>
            <th>Telefono</th>
            <th>Edad</th>
            <th>Activo</th>
            <th>Acciones</th>
        </tr>   
    </thread>    

    <tbody class="thread-light">
        @foreach($contactos as $contacto)
        <tr>
            <td>{{ $contacto->id }}</td>
            <td>{{ $contacto->name }}</td>
            <td>{{ $contacto->email }}</td>
            <td>{{ $contacto->phone }}</td>
            <td>{{ $contacto->age }}</td>
            <td>{{ $contacto->active }}</td>
            <td>
             
            <a href = "{{ url('/contac/'.$contacto->id.'/edit') }}">
            Edit
            </a>

            /

                <form action="{{ url('/contac/'.$contacto->id) }}" method="post">
                @csrf
                @method('DELETE') 
                    <input type="submit" onclick="return confirm('¿Desea Borrar?')" value="Borrar">
                </form>
            </td>
        </tr> 
        @endforeach  
    </tbody>   

</table>    